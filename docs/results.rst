*********************
Analizando resultados
*********************

Luego de seleccionar las opciones de filtrado correspondiente y Aplicar la búsqueda, los resultados se obtienen a través de la disposición que se muestra debajo.
La distribución general permite obtener información respecto a la cantidad de llamadas atendidas, cantidad de llamadas perdidas o no atendidas, y promedios en función del período seleccionado.

.. image:: images/results-generales.png
        :align: center

* **Generación del Reporte:** fecha y hora de generación del reporte.
* **Campañas:** nombres de las campañas pre-seleccionadas en el filtro general.
* **Período Inicial y Final:** rango de fechas sobre el cual se desplegará el reporte de actividad.
* **Dias de la semana:** dias pre-seleccionados sobre los que se analizará la información.
* **Rango Horario:** rango/s horario/s de análisis.
* **Total Llamadas Procesadas:** número de llamadas procesadas de acuerdo al filtrado inicial de reporte.
* **Porcentaje de Atendidas:** Porcentaje de Llamadas atendidas, respecto al número total de llamadas procesadas en el rango de fecha y hora seleccionado.
* **Porcentaje de No Atendidas:** Porcentaje de Llamadas No Atendidas, respecto al número total de llamadas procesadas en el rango de fecha y hora seleccionado.

Distribución general
*********************

Aquí se visualizan los contadores globales de llamadas:

* **Recibidas:** llamadas recibidas por la campaña.
* **Atendidas:** llamadas atendidas y derivadas a un agente
* **No Atendidas:** llamadas no atendidas.
* **Llamadas Abandonadas:** llamadas que fueron abandonadas antes de poder ser derivadas a un agente.
* **Transferidas:** llamadas transferidas por el agente hacia otros destinos.
* **Atendidas:** porcentaje de llamadas atendidas respecto al total de recibidas en campaña.
* **No atendidas:** porcentaje de llamadas no atendidas respecto al total de recibidas en campaña.
* **Tiempo Promedio de Espera:** tiempo promedio que la llamada debió esperar para ser atendida. Resulta de la división entre la suma de los tiempos de espera y el total de llamadas.
* **Tiempo Promedio al habla:** tiempo promedio de conversación. Resulta de la división entre la suma de tiempos al habla para cada llamada, y el total de llamadas atendidas.

Estos resultados pueden discriminarse por Campaña, por Rango Horario, por Mes, Semana, Dia de la Semana y Hora del dia. Cada Registro de campaña puede ampliarse clickeando en el signo “más” (+), a los fines de obtener mayor granularidad estadística.

El botón de “Descarga CSV”, permite descargar el output de la pestaña actual del reporte en el formato estándar CSV, para su correspondiente presentación y análisis en Excel o LibreOffice.

.. image:: images/results-distgeneral.png
        :align: center

.. image:: images/results-distgeneral2.png
        :align: center

Llamadas atendidas
*******************

En este apartado se ofrece una vista general de distribución de atendidas por campaña, de acuerdo al filtrado seleccionado. Sin embargo, el módulo ofrece mayor detalles de las transacciones de llamadas en los siguientes menúes del tópico:

.. image:: images/results-llamatendida.png
        :align: center

.. image:: images/results-llamatendida2.png
        :align: center

* **Generación del Reporte:** fecha y hora de generación del reporte.
* **Campañas:** nombres de las campañas pre-seleccionadas en el filtro general.
* **Período Inicial y Final:** rango de fechas sobre el cual se desplegará el reporte de actividad.
* **Dias de la semana:** dias pre-seleccionados sobre los que se analizará la información.
* **Rango Horario:** rango/s horario/s de análisis.
* **Total Llamadas Atendidas:** número de llamadas atendidas de acuerdo al filtrado inicial de reporte.

Detalle llamadas atendidas
###########################

Donde se visualizan los siguientes campos:

* **Fecha y Hora:** “datetime” de la llamada.
* **Número Telefónico:** callerID llamante.
* **Nombre de Campaña:** nombre de la campaña para la que se registra la transacción.
* **Tipo de campaña:** ID de tipo de campaña.
* **Nombre de Agente:** agente que procesó la llamada
* **Tiempo de espera:** tiempo que esperó el llamante para ser atendido por un agente.
* **Duración de la conversación:** tiempo al habla para la transacción telefónica.
* **Causa de Desconexión:** incorpora a la estadística “la fuente” del corte de la llamada o evento de hang up (si corta el cliente, o si corta el agente).
* **Calificación de la llamada:** calificación que guardó el agente para el cierre de dicha gestión.
* **Archivo de Grabación:** enlace para escucha online o descarga correspondiente de la grabación

.. note::

  La campaña requiere activar la grabación de llamadas para tal efecto

.. image:: images/results-detallellamatendida.png
        :align: center

Service level
#############

Este segmento permite observar la distribución de los tiempos de espera, distribuidos en bloques pre-definidos de segundos. Básicamente muestra la cantidad de llamadas que fueron atendidas dentro de un slot de tiempo (el bloque de
tiempo incremental puede ser definido en el filtro inicial de reporte).

.. image:: images/results-servicelevel.png
        :align: center

.. image:: images/results-servicelevel2.png
        :align: center

* **Atendidas antes de los X segundos:** todas aquellas llamadas atendidas dentro de los X segundos de espera, donde X se define en el filtrado inicial de reporte (parámetro Service Level).
* **Número:** número de llamadas que se atienden.
* **Delta:** valor de incremento “absoluto” para cada slot de tiempo, expresado en cantidad de llamadas.
* **Porcentaje:** porcentaje del total de llamadas recibidas, para cada slot de tiempo.

Atendidas por agente
####################

Donde se aprecian los detalles de:

* **Nombre de agente:** nombre del agente que atendió la llamada.
* **Número de Atendidas:** número de llamadas atendidas por el agente.
* **Transferidas:** Número de llamadas transferidas por el agente.
* **Atendidas:** Porcentaje de llamadas atendidas por el agente, respecto al total.
* **Tiempo Total al Habla:** tiempo en segundos, que el agente ha pasado al habla durante la operación.
* **Tiempo promedio al Habla:** tiempo promedio al habla, que resulta de dividir el tiempo total al habla sobre la cantidad de llamadas atendidas.
* El signo “más” (+) ofrece mayor granularidad de la llamada en cuestión.

.. image:: images/results-atendidasxagente.png
        :align: center

.. image:: images/results-atendidasxagente2.png
        :align: center

Causas de desconexión
######################

El sistema permite detectar si el corte de la llamada atendida proviene del Agente o del prospecto/cliente. De esta manera ofrece contadores por evento y porcentajes globales para el filtro seleccionado.

* **Causas:** el evento COMPLETEOUTNUM refiere a un corte proveniente del extremo remoto o cliente. Por otro lado COMPLETEAGENT refiere a un evento de corte proveniente del agente.
* **Número de llamadas:** número de transacciones que cayeron en cada evento de corte.
* **Porcentaje:** porcentaje de llamadas que caen dentro del evento de corte, respecto al total de llamadas procesadas.

.. image:: images/results-causasdesconexion.png
        :align: center

.. image:: images/results-causadesconexion2.png
        :align: center

Llamadas no atendidas
**********************

En este apartado se ofrece una vista general de distribución de no atendidas por campaña, de acuerdo al filtrado seleccionado. Es decir, contabiliza las llamadas perdidas que por diferentes motivos no pudieron ser conectadas a un agente (ya sea porque el llamante abandonó, o expiró el tiempo de espera en cola, o se obtuvo algún evento de falla al momento de enrutar la llamada).

Para ahondar en mayor granularidad, el módulo ofrece mayores detalles de las transacciones de llamadas en los siguientes menúes del tópico:

.. image:: images/results-noatendidas.png
        :align: center

* **Generación del Reporte:** fecha y hora de generación del reporte.
* **Campañas:** nombres de las campañas pre-seleccionadas en el filtro general.
* **Período Inicial y Final:** rango de fechas sobre el cual se desplegará el reporte de actividad.
* **Días de la semana:** días pre-seleccionados sobre los que se analizará la información.
* **Rango Horario:** rango/s horario/s de análisis.
* **Total Llamadas No Atendidas:** número de llamadas no atendidas de acuerdo al filtrado inicial de reporte.

Detalle llamadas no atendidas
#############################

Cada registro provee la siguiente información:

* **Fecha:** o “datetime” de la transacción telefónica.
* **Número Telefónico:** representa el CallerID llamante
* **ID Campaña:** Identificador de Campaña.
* **Nombre de Campaña:** nombre de la campaña donde se registra la transacción telefónica.
* **Tipo de Campaña:** ID de identificación de tipo de campaña.
* **ID de Agente:** Identificador de Agente.
* **Tiempo de Espera:** tiempo que debió esperar el cliente antes de finalizar la transacción.
* **Causas de no conexión:** motivo por el cual la llamada no se derivó a un agente.

.. image:: images/results-detallellamadanoatendida.png
        :align: center

Porcentaje de llamadas no atendidas
###################################

Los apartados ofrecen contadores y porcentajes tanto por campaña como por agente.

* **No Conexiones por Agente:** refiere a llamadas manuales no conectadas originadas por agentes.
* **No Conexiones por Campaña:** refiere llamadas no conectadas originadas por campaña.
* **Número:** número de llamadas no atendidas.
* **Porcentaje:** porcentaje de llamadas no conectadas respecto del total de no atendidas.

.. image:: images/results-porcentajenoatendida.png
        :align: center

.. image:: images/results-porcentajenoatendida2.png
        :align: center

Causas de No conexión
#####################

Donde se contabilizan los diferentes motivos que originaron la no-conexión de la llamada, en términos globales y porcentuales. Dentro de las causas de no conexión se presentan las más comunes:

* **EXITWITHTIMEOUT:** evento de corte por parte del cliente, luego de alcanzar el tiempo máximo de espera en cola.
* **ABANDON:** evento de corte por parte del cliente, luego de desistir a la espera en cola.
* **CONGESTION:** señal de congestión en los intentos de efectuar salientes.
* **NONDIALPLAN:** evento de falla producto de no encontrar una ruta saliente válida para la llamada que se intenta cursar.

.. image:: images/results-causasnoconexion.png
        :align: center

.. image:: images/results-causasnoconexion2.png
        :align: center

Reportes de agente
******************

Las estadísticas de agente resultan clave a la hora de determinar la performance del Contact Center y la Productividad de la dotación de Agentes.

Por dicho motivo se ofrece información detallada en términos de actividad y disponibilidad de agentes, métricas relevantes para acompañar y corregir el desempeño de la Operación.

.. image:: images/results-reportesagentes.png
        :align: center

.. image:: images/results-reportesagentes2.png
        :align: center

* **Agentes Disponibles:** cantidad de agentes disponibles para el rango de fecha y hora seleccionado.
* **Tiempo promedio de agente:** tiempo promedio de sesión, que resulta de dividir la suma de todas las sesiones sobre el total de sesiones detectado para el filtro seleccionado.
* **Tiempo mínimo de agente:** tiempo de sesión mínima registrado en la plataforma.
* **Tiempo máximo de agente:** tiempo de sesión de mayor duración registrado en la plataforma.
* **Tiempo Total de Agente:** Suma de Todos los tiempos de sesión registrados en filtro inicial.

Disponibilidad de Agente
########################

En este menú se registra información inherente a la actividad del agente, como ser tiempos de sesión, tiempos de pausa, tiempos al habla, etc..

* **ID:** Identificador de Agente
* **Agente:** nombre de agente.
* **Número de Sesiones:** cantidad de eventos de Login y Logout que se contabilizan para el rango de fecha y hora seleccionada.
* **Tiempo de Hold:** acumulado de tiempo en Hold para el filtro de fecha y hora seleccionada.
* **Tiempo Total de Sesion:** acumulado de tiempo de sesión para el filtro de fecha y hora seleccionada.
* **Promedio de Sesión:** promedio resultante de dividir la suma total de tiempos de sesión sobre la cantidad de sesiones.
* **Tiempo On Call:** Tiempo total en que el agente estuvo al habla.
* **Número de Pausas:** número total de eventos de pausa registradas.
* **Tiempo Total de Waiting:** tiempo total en el que el agente estuvo en espera, listo para recibir llamadas.
* **Tiempo Total de Pausa:** tiempo total en que el agente estuvo pausado.
* **Promedio de Pausa:** tiempo promedio de cada pausa, resultante de dividir el tiempo total de pausa sobre la cantidad de pausas registradas.
* **% de ocupación:** Porcentaje de ocupación del agente, resultante de dividir la cantidad de tiempo que estuvo al habla sobre el total de tiempo de sesión registrado.

.. image:: images/results-disponibilidadagente.png
        :align: center

* Clickeando sobre el agente, se obtiene mayor granularidad en la información sobre las sesiones y las pausas de agente.

.. image:: images/results-disponiblidadagente2.png
        :align: center

* Clickeando sobre el tiempo total de pausa, se obtiene información de los tiempos de pausa totales para cada pausa agrupadas en pausas Productivas y Recreativas.

.. image:: images/results-disponiblidadagente3.png
        :align: center


Número de agentes por día
#########################

Esta métrica permite tener un pantallazo general de la distribución de la dotación de agentes por día y hora de la semana dentro del rango seleccionado en el filtro del reporte.

.. image:: images/results-numeroagentesxdia.png
        :align: center

.. image:: images/results-numeroagentesxdia2.png
        :align: center

Eventos de resultados de llamadas
**********************************

+-------------------+---------------------------------------------------------------------------------------------+
| Log               | Description                                                                                 |
+===================+=============================================================================================+
| ABANDON           | Abandon of a call in a queue                                                                |
+-------------------+---------------------------------------------------------------------------------------------+
| ABANDON-CTOUT     | The customer abandoned the call in a CT is in progress between an agent an external number  |
+-------------------+---------------------------------------------------------------------------------------------+
| ABANDONWEL        | The customer abandons when is listening the entrance audio of queue                         |
+-------------------+---------------------------------------------------------------------------------------------+
| ANSWER            | Attended outbound calls                                                                     |
+-------------------+---------------------------------------------------------------------------------------------+
| BTOUT-ANSWER      | Transfer attended between an agent and external number                                      |
+-------------------+---------------------------------------------------------------------------------------------+
| BTOUT-TRY         | Transfer attempt between an agent and external number                                       |
+-------------------+---------------------------------------------------------------------------------------------+
| CANCEL            | Cancelled outbound calls                                                                    |
+-------------------+---------------------------------------------------------------------------------------------+
| CHANUNAVAIL       | Channel unavailable for outbound calls                                                      |
+-------------------+---------------------------------------------------------------------------------------------+
| COMPLETE-BTOUT    | Blind transfer attend to an external number                                                 |
+-------------------+---------------------------------------------------------------------------------------------+
| COMPLETE-CTOUT    | Consultative transfer to an external number                                                 |
+-------------------+---------------------------------------------------------------------------------------------+
| COMPLETEAGENT     | Agent hangups the call                                                                      |
+-------------------+---------------------------------------------------------------------------------------------+
| COMPLETEOUTNUM    | Customer hangups the call                                                                   |
+-------------------+---------------------------------------------------------------------------------------------+
| CT-ANSWER         | Attended consultative transfer                                                              |
+-------------------+---------------------------------------------------------------------------------------------+
| CT-BUSY           | Busy consultative transfer                                                                  |
+-------------------+---------------------------------------------------------------------------------------------+
| CT-DISCARD        | Consultative transfer discarded by agent                                                    |
+-------------------+---------------------------------------------------------------------------------------------+
| CT-TRY            | Try of consultative transfer                                                                |
+-------------------+---------------------------------------------------------------------------------------------+
| CTOUT-ABANDON     | Consultative transfer abandon to external numbers                                           |
+-------------------+---------------------------------------------------------------------------------------------+
| CTOUT-ACCEPT      | Consultative transfer accept of external numbers                                            |
+-------------------+---------------------------------------------------------------------------------------------+
| CTOUT-ANSWER      | Consultative transfer answer of external numbers                                            |
+-------------------+---------------------------------------------------------------------------------------------+
| CTOUT-CHANUNAVAIL | Channel unavailable of consultative transfer of external numbers                            |
+-------------------+---------------------------------------------------------------------------------------------+
| CTOUT-DISCARD     | Discard of consultative transfer of external numbers                                        |
+-------------------+---------------------------------------------------------------------------------------------+
| CTOUT-TRY         | Try of consultative transfer of external numbers                                            |
+-------------------+---------------------------------------------------------------------------------------------+
| DIAL              | Dial try of an outbound call                                                                |
+-------------------+---------------------------------------------------------------------------------------------+
| ENTERQUEUE        | Call ingress to a queue (for Inbound and Dialer)                                            |
+-------------------+---------------------------------------------------------------------------------------------+
| EXITWITHTIMEOUT   | Timeout in queue                                                                            |
+-------------------+---------------------------------------------------------------------------------------------+
| NOANSWER          | Outbound call not attended                                                                  |
+-------------------+---------------------------------------------------------------------------------------------+
| NONDIALPLAN       | No outbound route for outbound call                                                         |
+-------------------+---------------------------------------------------------------------------------------------+
| COMPLETE-CT       | Agent hangups counsultative transfer                                                        |
+-------------------+---------------------------------------------------------------------------------------------+

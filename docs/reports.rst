*********
Reportes
*********

Reportes de Actividad
**********************

Una vez que el Addon fue activado de manera satisfactoria, el menú de Administración
desplegará el apartado correspondiente:

.. image:: images/reportes-vistaoml.png
        :align: center

La primer pantalla ofrece un Panel de Filtrado amplio, que podemos dividirlo en 2 partes:

Parámetros Fijos
#################

.. image:: images/reportes-paramfijos.png
        :align: center

* **Tipo de Campaña:** permite seleccionar el tipo de campaña, si es Entrante, Saliente Manual, Saliente Dialer, Saliente Preview, Todas.
* **Campañas específicas:** el tipo de campaña pre-selecciona los nombres de campaña relacionados al tipo de campaña. Se puede seleccionar Todas, una o varias al mismo tiempo.
* **Grupos de Agente:** selecciona el grupo de agentes previamente creado por el Administrador.
* **Agentes específicos:** permite seleccionar agentes específicos de la/s campaña/s en cuestión.
* **Inclusión de Llamadas Manuales:** es posible excluir o incluir a gusto las llamadas del tipo Manual que hayan efectuado los agentes en nombre de la campaña.
* **Número Telefónico:** permite el filtrado por CallerID o número telefónico del prospecto.
* **CallID de Llamada:** vincula la variable de Asterisk que identifica la llamada, a los fines técnicos de seguimiento de canal.
* **Tipo de Conexión:** permite filtrar por las Fallidas (llamadas que no se pudieron establecer), las Exitosas (llamadas conectadas y satisfactorias), o Todas.

Parámetros Temporales
#####################

.. image:: images/reportes-paramtemp.png
        :align: center

* **Fecha de Inicio:** fecha de inicio de la búsqueda.
* **Fecha Fin:** fecha de fin de la búsqueda.
* **Dias de la Semana:** dia/s de la semana en la que se ejecuta el filtro.
* **Rango Horario contiguo y no contiguo:** permite agregar uno o más rangos horarios no contiguos, selección muy útil a la hora de obtener estadísticas por grupos horarios.
* **Ajuste de Zona Horaria:** parámetro de offset que se pre-selecciona con el objetivo de desfasar el horario en función de la región donde se encuentre el grupo de agentes en cuestión, y cuya zona horaria difiera de la correspondiente a la instancia.
* **Service Level:** permite la selección de un nivel de servicio distribuido en bloques de segundos, con el objetivo de contabilizar las llamadas atendidas en cada bloque de interés.

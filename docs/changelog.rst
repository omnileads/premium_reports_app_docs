**********
Changelog
**********

**Release 1.0.0:**
  - Initial release, possibility of use of all the features of addon
**Release 1.1.0:**
  - Compatibility with OMniLeads from release 1.6.0 to release 1.11.0
  - Added CallID field in details of calls
  - Fixed a bug that made the browser crash due to overload of response content when there were too much call data.
  - Fixed a bug that made show the reports in UTC timezone.
**Release 1.2.0:**
  - Erased the dependencie of use of recording table of OMniLeads database
**Release 1.2.1:**
  - Fixed error when building .po files
**Release 1.3.0:**
  - New Modal window showing agent pauses totals by Pause, distinguishing Productive from Recreational.
  - On Hold time is now reflected on reports.
  - Fixed calls attended by agent .csv report headers.
  - Better handling of possible inconsistences in agent event logs.
**Release 1.3.1:**
  - Distribution by day shows Weekday name.
  - Agent's full name shown in filters.
  - Fixed a bug that made the addon crash when HOLD logs from non filtered Agents where detected.
**Release 1.3.2:**
  - Show only agents related to filtered campaigns.
  - Show first login and last logout to Agents availability.
  - Queries optimization.
**Release 1.3.3:**
  - Fixes missing stats for calls without agent.
**Release 1.3.4:**
  - Adds descriptions to call events codes.
**Release 1.3.5:**
  - Adds agents usernames on several tables.
**Release 1.3.6:**
  - Fix historical dispositions are now listed in call details report.
  - Fix unnasigned agents are now included in reports if activity detected.
**Release 1.3.7:**
  - Major reports improvements.
  - Filter selects allows option searching.

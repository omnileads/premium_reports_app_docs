.. Omnileads Installation documentation master file, created by
   sphinx-quickstart on Wed Apr 30 10:52:05 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

********************************
OMniLeads Premium Reports Addon
********************************

Que es el Reporting Pro?
*************************

Dentro del ecosistema de extensiones y addons que pueden agregarse a OmniLeads, Reporting  Pro es un componente clave a la hora de obtener precisión en las mediciones analíticas de actividad de un Contact Center.
Este Addon permite generar múltiples métricas de productividad y eficiencia, a la vez que combina la información estadística con el objeto de ofrecer mayor “granularidad” a la hora del despliegue de información, lo que permite una mejora sustancial en la presentación de la información para la toma de decisiones.
Dentro del compendio de métricas existentes, se incluyen:

* Identificación de números llamantes o números discados
* Distribución por Campaña, Horario, Mes, Semana, Dia y Hora.
* Detalle de llamadas atendidas por Campaña, Agente, Horario
* Detalle de llamadas fallidas
* Número de intentos
* Tiempos de Waiting Totales y Promedios
* Tiempos al Habla Totales y Promedios
* Tiempos de Sesión Totales y Promedios
* Tiempos de Pausa Totales y Promedios
* Disponibilidad de Agentes: Tiempos de Inicio-Fin para Sesiones y Pausas
* Número de Agentes por Día y Hora
* Motivos de Desconexión para cada llamada establecida, por Campaña
* Motivos de No Conexión para cada intento de llamada, por Campaña
* Horarios de llamadas y destinos de enrutamiento, con sus correspondientes tiempos de espera y estados de finalización.
* Exhaustiva Granularidad de Filtrado: por Tipo de Campaña, Por Campaña, Por Grupos de Agentes y/o Agentes Invidivuales, Por teléfono y/o ID de Llamada, por Tipo de Conexión, por Franja Horaria continua o No Contigua.
* Ajustes por Zona horaria (ideal para sedes en zonas geográficas diferentes)
* Nivel de Servicio personalizable.
* Exportación de Resultados a CSV
* Descarga y Escucha de Grabaciones desde reporte.

Requisitos
***********

1. Una instancia de OMniLeads

- **release-1.5.0** minimamente instalado para el **release-1.1.0** del Premium Reports.
- **release-1.12.0** minimamente instalado para el **release-1.2.0** del Premium Reports.
- **release-1.14.0** minimamente instalado para el **release-1.3.0** del Premium Reports.
- **release-1.24.0** minimamente instalado para el **release-1.3.7** del Premium Reports.

.. toctree::
   :hidden:

   install.rst
   reports.rst
   results.rst
   changelog.rst
